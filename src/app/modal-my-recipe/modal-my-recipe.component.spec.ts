import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMyRecipeComponent } from './modal-my-recipe.component';

describe('ModalMyRecipeComponent', () => {
  let component: ModalMyRecipeComponent;
  let fixture: ComponentFixture<ModalMyRecipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalMyRecipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMyRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
