import { Component, OnInit } from '@angular/core';
import { RecipesService } from 'src/app/services/recipes.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private recipesService: RecipesService) { }

  recipe: any = {}

  recipes: any[] = [];

  ngOnInit(): void {
    this.getTopTen();
  }

  getTopTen () {
    this.recipesService.getTopTen().subscribe(
      res => {
        this.recipes = res;
      },
      err => console.log(err)
    );
  }

}
