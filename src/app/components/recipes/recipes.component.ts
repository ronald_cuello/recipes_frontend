import { Component, OnInit } from '@angular/core';
import { RecipesService } from 'src/app/services/recipes.service';
import { QualificationsService } from 'src/app/services/qualifications.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  recipe = {
    id: 0,
    name: '',
    description: '',
    preparation: '',
    note: '',
    ingredients: <any>[],
    images: <any>[]
  }

  recipes: any[] = [];
  copy: any[] = [];

  option: string = '1';
  
  rate: number = 0;
  selectedRecipe = 0;

  constructor(private recipesService: RecipesService, private qualificationsService: QualificationsService, public auth: AuthService) { }

  ngOnInit(): void {
    this.getRecipes();
  }

  refreshList() {
    switch(this.option){
      case '1':
        this.getRecipes();
        break;
      case '2':
        this.getMyRecipes();
        break;
    }

  }

  getRecipes() {
    this.recipesService.getRecipes().subscribe(
      res => {
        this.recipes = res;
        this.copy = this.recipes;
      },
      err => console.error(err)
    );
  }

  getMyRecipes() {
    this.recipesService.getMyRecipes().subscribe(
      res => {
        this.recipes = res;
        this.copy = this.recipes;
      },
      err => console.error(err)
    );
  }

  rateRecipe(){
    console.log(this.rate);
    this.qualificationsService.saveQualification({rate: this.rate, recipe_id: this.recipe.id}).subscribe(
      res => {
        console.log('rate', res);
      },
      err => console.error(err)
    );
  }

  verDetalles(r: any){
    this.recipesService.getById(r.id).subscribe(
      res => {
        this.recipe = res;
      },
      err => console.error(err)
    );
  }

  buscar(b: any){
    if (b.value != ''){
      this.recipes = this.copy.filter((i) => {
        return i.name.indexOf(b.value) >= 0
      });
      console.log('buscar:', this.recipes);
    }else{
      this.refreshList();
    }
  }


}
