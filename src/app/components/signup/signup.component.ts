import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  user = {
    names: '',
    lastnames: '',
    username: '',
    email: '',
    password: ''
  }

  constructor( private auth: AuthService, private router: Router ) { }

  ngOnInit(): void {
  }

  signup(){
    console.log(this.user);
    this.auth.signup(this.user).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/login']);
      },
      err => console.error(err)
    );
  }

}
