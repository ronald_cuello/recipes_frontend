import { Component, OnInit, ViewChild } from '@angular/core';
import { RecipesService } from 'src/app/services/recipes.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-recipes-creator',
  templateUrl: './recipes-creator.component.html',
  styleUrls: ['./recipes-creator.component.css']
})
export class RecipesCreatorComponent implements OnInit {

  editting = false;
  edit_id = 0;

  ingredient: any = {};
  ingredients: any[] = [];

  image: string = '';
  images: any[] = [];
  imagesUploaded: any[] = [];

  recipe = {
    name: '',
    description: '',
    preparation: '',
    note: '',
    ingredients: <any>[],
    images: <any>[]
  }

  recipes: any[] = [];

  constructor(private recipesService: RecipesService) { }

  ngOnInit(): void {
    this.getMyRecipes();
  }

  createRecipe() {
    if (this.recipe.name && this.recipe.description && this.recipe.preparation && this.recipe.note){
      this.recipe.ingredients = this.ingredients;

      this.upload();

    }
  }

  addIngredient(){
    if (this.ingredient.name){
      this.ingredients.push(this.ingredient);
      this.ingredient = {};
    }
  }

  removeIngredient(item: any){
    const r_ingredients = this.ingredients.filter((i) => {
      return i != item;
    });
    this.ingredients = r_ingredients;
  }

  imageUpload(e: any, element: any){
    if (e.target.files){
      this.imagesUploaded.push(e.target.files[0]);
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        this.image =  event.target.result;
        this.images.push(event.target.result);
        element.value = '';
      }
    }
  }

  upload(): any {
    try{
      
      const data = new FormData();
      try{
        this.imagesUploaded.forEach(img => {
          data.append('images', img);
        });
      }catch(e){
        console.log('error al subir archivo');
      }
      
      this.recipesService.uploadImages(data).subscribe(
        res => {
          this.recipe.images = res;

          this.recipesService.createRecipe(this.recipe).subscribe(
            res => {
              this.recipe = {
                name: '',
                description: '',
                preparation: '',
                note: '',
                ingredients: <any>[],
                images: <any>[]
              }
              this.ingredients = <any>[];
              this.images = <any>[];
              this.imagesUploaded = <any>[];

              this.getMyRecipes();

              Swal.fire(
                'Receta Guardada!',
                'Receta guardada con exito!',
                'success'
              )
    
            },
            err => console.error(err)
          );

        },
        err => console.error(err)
      );

    }catch(e){
      console.log('error al subir archivo');
    }
  }

  getMyRecipes(){
    this.recipesService.getMyRecipesPrivate().subscribe(
      res => {
        this.recipes = res;
      },
      err => console.error(err)
    );
  }

  Deactivate(id: number){
    this.recipesService.deactivate(id).subscribe(
      res => {
        console.log('Desactivando', res);
        if (res.id){
          this.closeEdit();
          this.getMyRecipes();
        }
      },
      err => console.error(err)
    );
  }

  Activate(id: number){
    this.recipesService.activate(id).subscribe(
      res => {
        console.log('Activando', res);
        if (res.id){
          this.getMyRecipes();
        }
      },
      err => console.error(err)
    );
  }

  Delete(id: number){
    Swal.fire({
      title: 'Eliminar receta?',
      text: "Si eliminas la receta no podrás restaurarla",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.recipesService.delete(id).subscribe(
          res => {
            console.log('Activando', res);
            if (res.id){
              this.getMyRecipes();
              Swal.fire(
                'Receta eliminada!',
                'La receta se eliminó permanentemente',
                'success'
              )
            }
          },
          err => console.error(err)
        );
      }
    })
  }

  initEdit (r: any){

    this.edit_id = r.id;
    this.editting = true;

    this.recipe = {
      name: '',
      description: '',
      preparation: '',
      note: '',
      ingredients: <any>[],
      images: <any>[]
    }
    this.ingredients = <any>[];
    this.images = <any>[];
    this.imagesUploaded = <any>[];


    this.recipe.name = r.name;
    this.recipe.description = r.description;
    this.recipe.preparation = r.preparation;
    this.recipe.note = r.note;

  }

  closeEdit(){
    this.edit_id = 0;
    this.editting = false;

    this.recipe = {
      name: '',
      description: '',
      preparation: '',
      note: '',
      ingredients: <any>[],
      images: <any>[]
    }
    this.ingredients = <any>[];
    this.images = <any>[];
    this.imagesUploaded = <any>[];
  }

  update() {
    if (this.recipe.name && this.recipe.description && this.recipe.preparation && this.recipe.note){
      this.recipesService.update(this.edit_id, this.recipe).subscribe(
        res => {
  
          this.closeEdit();
          this.getMyRecipes();
  
          Swal.fire(
            'Receta Actualizada!',
            'Receta actualizada con exito!',
            'success'
          )
  
        },
        err => console.error(err)
      );
    }
    
  }

}
