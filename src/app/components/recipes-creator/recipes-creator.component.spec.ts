import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipesCreatorComponent } from './recipes-creator.component';

describe('RecipesCreatorComponent', () => {
  let component: RecipesCreatorComponent;
  let fixture: ComponentFixture<RecipesCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecipesCreatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipesCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
