import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  private URI = 'http://localhost:4000/api/recipes';

  constructor(public http: HttpClient) { }

  getTopTen() {
    return this.http.get<any>(`${this.URI}/get/topTen`);
  }

  getRecipes(){
    return this.http.get<any>(`${this.URI}/get/actives`);
  }

  getMyRecipes(){
    return this.http.get<any>(`${this.URI}/get/myRecipes`);
  }

  getMyRecipesPrivate(){
    return this.http.get<any>(`${this.URI}/get/myRecipes/private`);
  }

  createRecipe(recipe: any){
    return this.http.post<any>(`${this.URI}/createRecipe`, recipe);
  }

  uploadImages(data: any){
    return this.http.post<any>(`${this.URI}/uploadImages`, data);
  }

  update(id: number, data: any){
    return this.http.put<any>(`${this.URI}/${id}`, data);
  }

  deactivate(id: number){
    return this.http.put<any>(`${this.URI}/deactivate/${id}`, {});
  }

  activate(id: number){
    return this.http.put<any>(`${this.URI}/activate/${id}`, {});
  }

  delete(id: number){
    return this.http.delete<any>(`${this.URI}/${id}`, {});
  }

  getById(id: number){
    return this.http.get<any>(`${this.URI}/${id}`);
  }

}
