import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QualificationsService {

  private URI = 'http://localhost:4000/api/qualifications';

  constructor(public http: HttpClient) { }

  saveQualification(data: any){
    return this.http.post<any>(`${this.URI}/`, data);
  }

}
